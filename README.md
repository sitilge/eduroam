# eduroam

A trivial netctl profile for eduroam.

## Usage

Change the `someinterface`, `CID@chalmers.se`, `somepassword` according to your needs.

````
description='eduroam wireless WPA connection'
Interface=someinterface
Connection=wireless
Security=wpa-configsection
IP=dhcp
TimeoutWPA=30
WPAConfigSection=(
	'ssid="eduroam"'
	'key_mgmt=WPA-EAP'
	'eap=PEAP'
	'phase2="auth=MSCHAPV2"'
	'identity="CID@chalmers.se"'
	'password="somepassword"'
)
````
